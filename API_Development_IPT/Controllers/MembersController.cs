﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_Development_IPT.Models;

namespace API_Development_IPT.Controllers
{
    public class MembersController : ApiController
    {
        api_databaseEntities entities = new api_databaseEntities();
        // GET: api/Members
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Members/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Members
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Members/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Members/5
        public void Delete(int id)
        {
        }

        [Route("PostNewStudent")]
        [AcceptVerbs("POST", "GET")]
        public string Post([FromUri]string Email, string Name, string Password)
        {
            string WebOutput;
            STUDENT_MAST details = new STUDENT_MAST();
            string StudenId = "STD00";

            using (entities)
            {
                var query = (from info in entities.STUDENT_MAST
                             where info.STUDENT_EMAIL == Email
                             select info);
                if (query.Any())
                {
                    WebOutput = "User is Already Registered";
                }
                else
                {
                    var query2 = (from info in entities.STUDENT_MAST
                                  select info);
                    if (query2.Any())
                    {
                        int CountID = query2.Count();
                        StudenId = StudenId + Convert.ToString(CountID);
                    }

                    details.STUDENT_ID = StudenId;
                    details.STUDENT_EMAIL = Email;
                    details.STUDENT_NAME = Name;
                    details.STUDENT_PASSWORD = Password;

                    entities.STUDENT_MAST.Add(details);
                    entities.SaveChanges();

                    WebOutput = "Successfuly Registered";
                }
            }

            return WebOutput;
        }
    }
}
