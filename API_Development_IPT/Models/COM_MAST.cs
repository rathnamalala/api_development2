//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API_Development_IPT.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class COM_MAST
    {
        public string AUTH_ID { get; set; }
        public string MEMBER_ID { get; set; }
        public string IT_EXPERT_ID { get; set; }
        public string MEMBER_MESSAGES { get; set; }
        public string IT_EXPERT_MESSAGES { get; set; }
        public Nullable<System.DateTime> COM_DATE { get; set; }
    }
}
